package com.pluralsight.conferencedemo.models;

import com.pluralsight.conferencedemo.repositories.TimeSlotsJpaRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TimeSlotsTest {
    @Autowired
    TimeSlotsJpaRepository timeSlotsJpaRepository;

    @Test
    @DisplayName("Jpa Before")
    public void testJpaBefore(){
        List<TimeSlots> timeSlotsList = timeSlotsJpaRepository.findByTimeSlotDateBefore(new Date());

        assertTrue(timeSlotsList.size() > 0);
    }

    @Test
    @DisplayName("Jpa After")
    public void testJpaAfter(){
        List<TimeSlots> timeSlotsList = timeSlotsJpaRepository.findByTimeSlotDateAfter(new Date(1577328783000L));

        assertTrue(timeSlotsList.size() > 0);
    }


    @Test
    @DisplayName("Jpa Between")
    public void testJpaBetween(){
        List<TimeSlots> timeSlotsList = timeSlotsJpaRepository.findByTimeSlotDateBetween(new Date(1577328783000L),new Date());

        assertTrue(timeSlotsList.size() > 0);
    }



}