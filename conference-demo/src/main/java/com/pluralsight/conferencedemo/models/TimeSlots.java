package com.pluralsight.conferencedemo.models;

import javax.persistence.*;
import java.util.Date;
import java.sql.Time;

@Entity
@Table(name = "time_slots")
public class TimeSlots {
    @Id
    @Column(name = "time_slot_id")
    private Long timeSlotId;

    @Column(name = "time_slot_date")
    private Date timeSlotDate;

    @Column(name = "start_time")
    private Time startTime;

    @Column(name = "end_time")
    private Time endTime;

    @Column(name = "is_keynote_time_slot")
    private Boolean isKeynoteTimeSlot;

    public Long getTimeSlotId() {
        return this.timeSlotId;
    }

    public void setTimeSlotId(Long timeSlotId) {
        this.timeSlotId = timeSlotId;
    }

    public Date getTimeSlotDate() {
        return this.timeSlotDate;
    }

    public void setTimeSlotDate(Date timeSlotDate) {
        this.timeSlotDate = timeSlotDate;
    }

    public Time getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    public Boolean getIsKeynoteTimeSlot() {
        return this.isKeynoteTimeSlot;
    }

    public void setIsKeynoteTimeSlot(Boolean isKeynoteTimeSlot) {
        this.isKeynoteTimeSlot = isKeynoteTimeSlot;
    }
}
