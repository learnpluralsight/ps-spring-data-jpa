package com.pluralsight.conferencedemo.repositories;

import com.pluralsight.conferencedemo.models.Session;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SessionJpaRepository extends JpaRepository<Session,Long>, SessionCustomJpaRepository {
    List<Session> findBySessionNameContains(String name);
    Session findFirstBySessionNameContains(String name);
    Long countBySessionNameContains(String name);
    List<Session> findBySessionLengthNot(Integer sessionLenght);
    List<Session> findBySessionNameNotLike(String name);
    List<Session> findBySessionLengthLessThan(Integer sessionLenght);

    @Query("select s from Session s where s.sessionName like %?1")
    List<Session> getSessionsWithName(String name);

    @Query(value = "select * from session s where s.session_name = ?0", nativeQuery = true)
    List<Session> getSessionWithNameQN(String name);

    @Query("select s from Session s where s.sessionName like %:name")
    Page<Session> getSessionsWithNamePage(@Param("name") String name, Pageable pageable);

    @Modifying
    @Query("update Session s set s.sessionName = ?1")
    int updateSessionName(String name);
}
