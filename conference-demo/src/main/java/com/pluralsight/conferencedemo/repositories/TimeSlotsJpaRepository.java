package com.pluralsight.conferencedemo.repositories;

import com.pluralsight.conferencedemo.models.TimeSlots;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface TimeSlotsJpaRepository extends JpaRepository<TimeSlots, Long> {
    List<TimeSlots> findByTimeSlotDateBefore(Date startDate);
    List<TimeSlots> findByTimeSlotDateAfter(Date startDate);
    List<TimeSlots> findByTimeSlotDateBetween(Date startDate, Date endDate);
}
